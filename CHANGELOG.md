1.0.14

* Add Louisiana Creole (lou) translation by @nvjq
* Add Malay (ms-MY) by @Thaza_Kun
* Add Norwegian Nynorsk (nn-NO) by @jhsoby
* Add Norwegian Bokmål (nb-NO) by @jhsoby

1.0.13

* Add Arabic (ar-sa) translation. Thanks to @Shaata81

1.0.12

* Corrections to Palembang Malay (mui) translation. Thanks to Fletcher Hardison
* Add finnish (fi-fi) translation. Thanks to @susannaanas

1.0.11

* Add Romanian (ro) translation. Thanks to @diana_spoiala

1.0.10

* Add Palembang Malay (mui) translation. Thanks to Fletcher Hardison
* Add Brazilian Portuguese (pt-br) translation. Thanks to @lwksxns
* Add indonesian translation (id-id). Thanks to Anna Tasya Ardya

1.0.9

* Add Corrections on German/Switzerland (de-ch) language. Thanks to @cptSwing
* Add Spanish/Argentinia (es-AR). Thanks to @fiebredearchivo
* Add Portuguese translation. Thanks to @MMaia2021

1.0.8

* Add portuguese translation. Thanks to @MMaia2021
* Increase time on woman from 3s to 5s

1.0.7

* Malayalam / India (ml-in). Thanks to @AquaRegis32
* Dutch (nl-nl). Thanks to Jesse Vermeulen
* Latvian (lv). Thanks to @Robert2909

1.0.6

* Correct the italian (it-ch) translation. Thanks to Pietro Monticone for the updates

1.0.3

* Added (es) Spanish translation and (es-cl) Spanish translation with a Chilean flavour. Thanks to @guolivar for contribution.
* Added (fa-ir) Farsi translation (Persian/Iran). Awesome to see at this language wroten from right to left. Thanks to @nickokapi for contribution.

1.0.1

* change credit from @thespinofftv to thespinoff.co.nz

1.0.0

* Add Franch (fr-ch) translation.
* Add German (de-ch) translation.
* Add Italian (it-ch) translation.

Common Issues

* Please enable 'Allow commits from members who can merge to the target branch' by 'edit' in the top of this merge request.
* Please try to better align your texts

Message

New translations of the #FlattenTheCurve graphic by 
@SiouxsieW and @XTOTL 

* 

Thanks a lot to our translators !

https://gitlab.com/msutter/covid-19-curves-graphic-social/-/releases